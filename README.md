# The Good Dogs Project

A Hugo-based site using Bryan's theme for the Good Docs Project's Git training workshop.

Hosted on Netlify: [https://the-good-dogs-project-april-2023.netlify.app/](https://the-good-dogs-project-april-2023.netlify.app/)

Agenda for the training: [https://the-good-dogs-project-april-2023.netlify.app/agenda/](https://the-good-dogs-project-april-2023.netlify.app/agenda/)

Student homework: [https://the-good-dogs-project-april-2023.netlify.app/homework/](https://the-good-dogs-project-april-2023.netlify.app/homework/)